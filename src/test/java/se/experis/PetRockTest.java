package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    void setUp() {
        rocky = new PetRock("Rocky");
    }

    @Test
    void getName() {
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    void testUnhappyToStart() {
        assertFalse(rocky.isHappy());
    }

    @Test
    void testHappyAfterPlay() {
        rocky.playWithRock();
        assertTrue(rocky.isHappy());
    }

    @Disabled ("Exception throwing not yet defined")
    @Test
    void nameFail() {
        assertThrows(IllegalStateException.class,
                () -> rocky.getHappyMessage());
    }

    @Test
    void name() {
        rocky.playWithRock();
        String msg = rocky.getHappyMessage();
        assertEquals("I'm happy!", msg);
    }

    @Test
    void testFavNum() {
        assertEquals(42, rocky.getFaveNumber());
    }

    @Test
    void emptyNameFail() {
        assertThrows(IllegalArgumentException.class,
                () -> {PetRock woofy = new PetRock("");});
    }
}